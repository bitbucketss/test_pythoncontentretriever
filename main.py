import os

# defining the methods
def contentRetriever(filename):
    with open(filename,"r") as f:
        fileContent=f.read()
    if "pythondatascience" in fileContent.lower():
        return True

    else:
        return False


if __name__=="__main__":
  #getting the directory contents
   dir_contents=os.listdir()
   #iterating through the contents and retrieving the txt file
   nFile=0
   for item in dir_contents:
     if item.endswith('txt'):
      print(f"Detecting the contents : {item}")
      flag=contentRetriever(item)
      if (flag):
         nFile+=1
         print(f"content found-----> {item}")
      else:
        print(f"content not found {item}")

print(f"{nFile} files found !!!!!!!!!!!!")
